import Vue from 'vue'
import App from './App.vue'
import VueCarousel from 'vue-carousel';
import VueMeta from 'vue-meta'
 
Vue.use(VueCarousel);
Vue.use(VueMeta);

Vue.config.productionTip = false

new Vue({
  render: h => h(App),
}).$mount('#app')
